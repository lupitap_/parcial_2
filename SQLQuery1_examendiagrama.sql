create database productos1
use productos1;

create table tam(
id int Identity(1,1) Primary key Not null,
codigotamaņo float,
clasificacion integer,
);
create table producto(
id int Identity(1,1) Primary key Not null,
nombreproducto integer,
otrosdatos integer,
);
create table coloresproducto(
id int Identity(1,1) Primary key Not null,
);
create table color(
id int Identity(1,1) Primary key,
codigocolor float,
nombrecolor integer,
);
create table categoriasproducto(
id int Identity(1,1) Primary key Not null,
);
create table categorias(
id int Identity(1,1) Primary key,
categoriaprincipal float,
nombrecategoria integer,
);

select * from tam, producto, coloresproducto, color, categoriasproducto, categorias  